import datetime
from asyncio import sleep
from telethon import events
from telethon.errors.rpcerrorlist import YouBlockedUserError
from telethon.tl.functions.account import UpdateNotifySettingsRequest
from userbot import bot, CMD_HELP
from userbot.events import register

@register(outgoing=True, pattern="^\.qt(?: |$)(.*)")
async def _(event):
    if event.fwd_from:
        return
    reply_message = await event.get_reply_message() 
    if not reply_message.text:
       await event.edit("```Reply to text message```")
       return
    chat = "@es3n1n_bot"
    sender = reply_message.sender
    await event.edit("```Making a Quote```")
    async with bot.conversation(chat) as conv:
          try:     
              response = conv.wait_event(events.NewMessage(incoming=True,from_users=810547723))
              message = await bot.forward_messages(chat, reply_message)
              await message.reply("/quote")

              await sleep(4)
              response = await response 
          except YouBlockedUserError: 
              await event.reply("```Please unblock @es3n1n_bot and try again```")
              return
          if response.text.startswith("Hi!"):
             await event.edit("```Can you kindly disable your forward privacy settings for good?```")
          else: 
             await event.delete()   
             await bot.forward_messages(event.chat_id, response.message)

CMD_HELP.update({
        "quote": 
        ".qt \
          \nUsage: Enhance ur text to sticker.\n"
    })
