FROM dasbastard/ubuntu:latest

#
# Clone repo and prepare working directory
#
RUN git clone -b master https://gitlab.com/jarviscoldbox/xuserbot /root/userbot
RUN mkdir /root/userbot/bin/
WORKDIR /root/userbot/

CMD ["python3","-m","userbot"]
